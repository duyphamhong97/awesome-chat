//
//  Supportive.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import Foundation

enum AppError: Error, LocalizedError {
    case `default`(_ error: Error? = nil, status: Int? = nil, data: Data? = nil, description: String? = nil)
    case server(_ error: Error? = nil, status: Int? = nil, data: Data? = nil, description: String? = nil)
    case unexpected(_ error: Error? = nil, statusCode: Int? = nil, data: Data? = nil, description: String? = nil)
    
    var failureReason: String? {
        switch self {
        case .server:
            return NSLocalizedString("A server error has occurred", comment: "")
        case .unexpected:
            return ""
        default:
            return NSLocalizedString("The operation couldn't be completed", comment: "")
        }
    }
    
    var recoverySuggestion: String? {
        switch self {
        case .server:
            return NSLocalizedString("Please try again later.", comment: "")
        case .unexpected:
            return NSLocalizedString("Please check your internet connection", comment: "")
        default:
            return ""
        }
    }
}
