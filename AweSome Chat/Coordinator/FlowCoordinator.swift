//
//  FlowCoordinator.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import UIKit

public typealias CoordinatorStartCompletionHandler = () -> ()

protocol FlowCoordinator {
    var navigationController: UINavigationController { get }
    func start(completionHandler: CoordinatorStartCompletionHandler?)
}
