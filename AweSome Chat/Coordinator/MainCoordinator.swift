//
//  MainCoordinator.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import UIKit

protocol MainCoordinatorDIContainer {
    func makeSplashViewController(actions: SplashCoordinatorActions) -> SplashViewController
}

class MainCoordinator: FlowCoordinator {
    
    // MARK: - Properties
    let navigationController: UINavigationController
    let dependencyContainer: MainCoordinatorDIContainer
    
    init(navigationController: UINavigationController, dependencyContainer: MainCoordinatorDIContainer) {
        self.navigationController = navigationController
        self.dependencyContainer = dependencyContainer
    }
    
    func start(completionHandler: CoordinatorStartCompletionHandler?) {
        showSplash()
    }
    
    private func showSplash() {
        let actions = SplashCoordinatorActions(showLogin: showLogin)
        let splashVC = dependencyContainer.makeSplashViewController(actions: actions)
        navigationController.pushViewController(splashVC, animated: true)
    }
    
    private func showLogin() {
        let actions = LoginCoordinatorActions(showRegister: showRegister, showForgotPasscode: showForgotPasscode, showHome: showHome)
        let viewModel = LoginViewModel(coordinator: actions)
        let loginVC = LoginViewController(viewModel: viewModel)
        navigationController.pushViewController(loginVC, animated: true)
    }
    
    private func showRegister() {
        let actions = RegisterCoordinatorActions(backtoLogin: backToLogin)
        let viewModel = RegisterViewModel(coordinator: actions)
        let homeVC = RegisterViewController(viewModel: viewModel)
        navigationController.pushViewController(homeVC, animated: true)
    }
    
    private func showHome() {
        let actions = HomeCoordinatorActions()
        let viewModel = HomeViewModel(coordinator: actions)
        let homeVC = HomeViewController(viewModel: viewModel)
        navigationController.pushViewController(homeVC, animated: true)
    }
    
    private func showForgotPasscode() {
        
    }
    
//    func showPopUpResshowultRegister(isSuccess: Bool, descriptionError: String?) {
//        var title = ""
//        var description = ""
//        if isSuccess {
//            title = L10n.Register.RegisterSuccess.title
//            description = "Đăng nhập nhé"
//        } else
//        {
//            title = L10n.Register.RegisterFailed.title
//            description = descriptionError ?? ""
//        }
//        let alert = UIAlertController(title: title, message: description, preferredStyle: UIAlertController.Style.alert)
//
//        // add an action (button)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
//            isSuccess ? self.navigationController.popViewController(animated: true) : nil
//        }))
//
//        // show the alert
//        navigationController.present(alert, animated: true, completion: nil)
//    }
    
    func backToLogin() {
        navigationController.popViewController(animated: true)
    }
}
