//
//  AppDIContainer.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import UIKit

class DIContainer {
    // MARK: - Network
    
//    lazy var apiInteraction: ApiInteractor = {
//        let urlSesstion = URLSession.shared
//        let networkService = NetworkService(urlSession: urlSesstion)
//        return URLSessionAPIInteractor(with: networkService)
//    }()
    
    // MARK: - Repositories
    
//    func makeAuthenRepository() -> AuthenRepository {
//        return DefaultAuthenRepository(apiInteractor: apiInteraction)
//    }
    
    // MARK: - Services
    
//    lazy var authenService: AuthenSer = <#expression#>
    
    // MARK: - Flow Coordinators
    
    func makeMainCoordinator(navigationController: UINavigationController) -> MainCoordinator {
        return MainCoordinator(navigationController: navigationController, dependencyContainer: self)
    }
}

extension DIContainer: MainCoordinatorDIContainer {
    func makeSplashViewController(actions: SplashCoordinatorActions) -> SplashViewController {
        return SplashViewController(actions: actions)
    }
}
