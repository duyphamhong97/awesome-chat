//
//  AppCoordinator.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import UIKit

class AppCoordinator: FlowCoordinator {
    
    // MARK: - Properties
    lazy var navigationController = UINavigationController()
    let dependencyContainer: DIContainer
    
    // MARK: - Init
    init(dependencyContainer: DIContainer) {
        self.dependencyContainer = dependencyContainer
    }
    
    func start(completionHandler: CoordinatorStartCompletionHandler?) {
        let mainCoordinator = dependencyContainer.makeMainCoordinator(navigationController: navigationController)
        mainCoordinator.start(completionHandler: nil)
    }
}
