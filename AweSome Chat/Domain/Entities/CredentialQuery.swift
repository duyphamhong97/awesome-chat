//
//  CredentialQuery.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import Foundation

struct CredentialQuery: Equatable {
    let email: String
    let password: String
}
