//
//  AuthenRepository.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import Foundation
import FirebaseAuth

typealias AuthenDataResult = Result<AuthDataResult, AppError>

protocol AuthenRepository {
    
    func registerAccount(_ credential: CredentialQuery) async -> AuthenDataResult
    func login(_ credential: CredentialQuery) async -> AuthenDataResult
}
