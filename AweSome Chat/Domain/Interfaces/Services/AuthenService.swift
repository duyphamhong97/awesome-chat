//
//  AuthenService.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import Foundation

protocol AuthenServices {
    func register(_ credential: CredentialQuery) async throws -> AuthenDataResult
    func login(_ credential: CredentialQuery) async throws -> AuthenDataResult
}
