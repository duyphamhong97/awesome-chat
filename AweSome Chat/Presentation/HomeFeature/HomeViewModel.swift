//
//  HomeViewModel.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import Foundation
import RxSwift
import RxCocoa
import FirebaseAuth

struct HomeCoordinatorActions {
    
    
}

class HomeViewModel {
    
    private var coordinator: HomeCoordinatorActions?
    private let disposeBag = DisposeBag()
    
    init(coordinator: HomeCoordinatorActions) {
        self.coordinator = coordinator
    }
    
//    func transForm(input: Input) -> Output {
//
//    }
}

extension HomeViewModel {
    struct Input {
        
    }
    
    struct Output {
        
    }
}
