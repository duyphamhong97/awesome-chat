//
//  RegisterViewModel.swift
//  AweSome Chat
//
//  Created by pham hong duy on 02/06/2024.
//

import Foundation
import RxSwift
import RxCocoa
import FirebaseAuth

struct RegisterCoordinatorActions {
    let backtoLogin: () -> ()
}

class RegisterViewModel {
    
    private let coordinator: RegisterCoordinatorActions
    private let disposeBag = DisposeBag()
    
    init(coordinator: RegisterCoordinatorActions) {
        self.coordinator = coordinator
    }
    
    func transForm(input: Input) -> Output {
        
        let usernameValid = input.userName
            .map { $0.count >= 6 && $0.count <= 15 }
            .share(replay: 1)
        
        let emailValid = input.email
            .map { $0.isValidEmail() }
            .share(replay: 1)
        
        let passwordValid = input.password
            .map { $0.isValidPassword() }
            .share(replay: 1)
        
        
        let everythingValid = Observable.combineLatest(usernameValid, emailValid, passwordValid) { $0 && $1 && $2}
        
        let showErrors = input.registerTap.withLatestFrom(Observable.just(true))
        
        let usernameError = Observable.combineLatest(usernameValid, showErrors.startWith(false)) { isValid, shouldShowError in
            shouldShowError && !isValid ? "Username must be 6-15 characters." : nil
        }.asDriver(onErrorJustReturn: nil)
        
        let emailError = Observable.combineLatest(emailValid, showErrors.startWith(false)) { isValid, shouldShowError in
            shouldShowError && !isValid ? "Email is not valid." : nil
        }.asDriver(onErrorJustReturn: nil)
        
        let passwordError = Observable.combineLatest(passwordValid, showErrors.startWith(false)) { isValid, shouldShowError in
            shouldShowError && !isValid ? "Password must be 6-15 characters, contain at least one digit and one special character." : nil
        }.asDriver(onErrorJustReturn: nil)
        
        let registerResult = input.registerTap.withLatestFrom(Observable.combineLatest(everythingValid, input.email, input.password))
            .flatMapLatest { isValid, username, password -> Observable<(Bool, String, String)> in
                return Observable.just((isValid, username, password))
            }.asDriver(onErrorJustReturn: (false, "",""))
        
        registerResult
            .drive (onNext: { (success, email, password) in
                if success {
                    Auth.auth().createUser(withEmail: email, password: password) { [weak self] authResult, error in
                        guard let wSelf = self else { return }
                        guard let error = error else {
//                            wSelf.coordinator.showPopupRegister(true, nil)
                            return
                        }
                        wSelf.coordinator.backtoLogin()
//                        wSelf.coordinator.showPopupRegister(false, error.localizedDescription)
                    }
                }
            })
            .disposed(by: disposeBag)
        
        let movetoLogin = Observable.merge(input.loginTap, input.backTap)
            .asDriver(onErrorDriveWith: .empty())
        
        movetoLogin.drive(onNext: { [weak self] in
            self?.coordinator.backtoLogin()
        }).disposed(by: disposeBag)
        
        return Output(
            registerResult: registerResult,
            movetoLogin: movetoLogin,
            usernameError: usernameError,
            emailError: emailError,
            passwordError: passwordError
        )
    }
}

extension RegisterViewModel {
    struct Input {
        let userName: Observable<String>
        let email: Observable<String>
        let password: Observable<String>
        let termConditionTap: PublishSubject<Bool>
        let registerTap: Observable<Void>
        let loginTap: Observable<Void>
        let backTap: Observable<Void>
    }
    
    struct Output {
        let registerResult: Driver<(Bool, String, String)>
        let movetoLogin: Driver<Void>
        
        let usernameError: Driver<String?>
        let emailError: Driver<String?>
        let passwordError: Driver<String?>
    }
}
