//
//  RegisterViewController+UI.swift
//  AweSome Chat
//
//  Created by pham hong duy on 02/06/2024.
//

import Foundation
import UIKit

extension RegisterViewController {
    func setupUI() {
        setupTitle()
        setupAtribute()
    }
    
    func setupTitle() {
        registerTitle.text = L10n.Login.registerTitle
        userNameTitle.text = L10n.Login.username.uppercased()
        titleEmaillabel.text = L10n.Login.email
        passwordTitle.text = L10n.Login.password
                
        let combinedString = NSMutableAttributedString()
        combinedString.append("\(L10n.Login.agree) ".withAttributes(font: .typography(token: .latoRegular, size: 14), color: Asset.Colors._999999.color))
        combinedString.append(L10n.Login.term.withAttributes(font: .typography(token: .latoBold, size: 14), color: Asset.Colors._4356B4.color))
        combinedString.append(" \(L10n.Login.and) ".withAttributes(font: .typography(token: .latoRegular, size: 14), color: Asset.Colors._999999.color))
        combinedString.append(L10n.Login.condition.withAttributes(font: .typography(token: .latoBold, size: 14), color: Asset.Colors._4356B4.color))
            
        
        termConditionLabel.attributedText = combinedString
        registerButton.setTitle(L10n.Login.registerTitle.uppercased(), for: .normal)
        
        loginNowButton.setAttributedTitle(
            fullText: L10n.Login.haveAccount + " \(L10n.Login.loginNow)",
            parts: [
                (text:  L10n.Login.haveAccount, font: .typography(token: .latoRegular, size: 14), color: Asset.Colors._999999.color),
                (text: L10n.Login.loginNow, font: .typography(token: .latoBold, size: 14), color: Asset.Colors._4356B4.color)
            ]
        )
    }
    
    func setupAtribute() {
        backButton.setTitle("", for: .normal)
        backIcon.image = Asset.Assets.icBack.image
        iconUserName.image =  Asset.Assets.icUser.image
        emailIcon.image = Asset.Assets.icMail.image
        passwordIcon.image = Asset.Assets.icKey.image
        
        termCondition.setImage(Asset.Assets.icCheckEmpty.image, for: .normal) 
        termCondition.tintColor = Asset.Colors.cacaca.color
        termCondition.setTitle("", for: .normal)
        
        registerButton.setTitleColor(Asset.Colors.ffffff.color, for: .normal)
        registerButton.titleLabel?.font = .typography(token: .latoBold, size: 16)
    }
}
