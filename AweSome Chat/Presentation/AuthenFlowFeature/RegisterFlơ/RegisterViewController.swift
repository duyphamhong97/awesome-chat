//
//  RegisterViewController.swift
//  AweSome Chat
//
//  Created by pham hong duy on 02/06/2024.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backIcon: UIImageView!
    @IBOutlet weak var registerTitle: UILabel!
    @IBOutlet weak var userNameTitle: UILabel!
    @IBOutlet weak var iconUserName: UIImageView!
    @IBOutlet weak var userNameInputField: UITextField!
    @IBOutlet weak var titleEmaillabel: UILabel!
    @IBOutlet weak var emailInputField: UITextField!
    @IBOutlet weak var emailIcon: UIImageView!
    @IBOutlet weak var passwordTitle: UILabel!
    @IBOutlet weak var passwordInputField: UITextField!
    @IBOutlet weak var passwordIcon: UIImageView!
    @IBOutlet weak var termCondition: UIButton!
    @IBOutlet weak var termConditionLabel: UILabel!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loginNowButton: UIButton!
    
    // Error
    @IBOutlet weak var errorUsername: UILabel!
    @IBOutlet weak var errorEmail: UILabel!
    @IBOutlet weak var errorPassword: UILabel!
    
    let viewModel: RegisterViewModel
    private let disposeBag = DisposeBag()
    var isChecked = false

    init(viewModel: RegisterViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindViewModel()
    }
    
    private func bindViewModel() {
        let terConditionSubject = PublishSubject<Bool>()
        
        let input = RegisterViewModel.Input(userName: userNameInputField.rx.text.orEmpty.asObservable(),
                                            email: emailInputField.rx.text.orEmpty.asObservable(),
                                            password: passwordInputField.rx.text.orEmpty.asObservable(),
                                            termConditionTap: terConditionSubject,
                                            registerTap: registerButton.rx.tap.asObservable(),
                                            loginTap: loginNowButton.rx.tap.asObservable(),
                                            backTap: backButton.rx.tap.asObservable()
        )
                
        let output = viewModel.transForm(input: input)
        
        termCondition.rx.tap
            .subscribe(onNext: { [unowned self] _ in
                isChecked.toggle()
                termCondition.setImage(isChecked ? Asset.Assets.icChecked.image : Asset.Assets.icCheckEmpty.image, for: .normal)
                registerButton.backgroundColor = isChecked ? Asset.Colors._4356B4.color : Asset.Colors.cacaca.color
                registerButton.isEnabled = isChecked
            })
            .disposed(by: disposeBag)
                        
        output.usernameError
            .drive(onNext: { [weak self] error in
                self?.errorUsername.text = error ?? " "
                self?.errorUsername.isHidden = error == nil
            })
            .disposed(by: disposeBag)
        
        output.emailError
            .drive(onNext: { [weak self] error in
                self?.errorEmail.text = error ?? " "
                self?.errorEmail.isHidden = error == nil
            })
            .disposed(by: disposeBag)
        
        output.passwordError
            .drive(onNext: { [weak self] error in
                self?.errorPassword.text = error ?? " "
                self?.errorPassword.isHidden = error == nil
            })
            .disposed(by: disposeBag)
    }
}
