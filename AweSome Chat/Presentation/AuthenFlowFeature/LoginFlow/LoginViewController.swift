//
//  LoginViewController.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 5/31/24.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class LoginViewController: UIViewController {
    
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var screenNameLabel: UILabel!
    @IBOutlet weak var titleEmaillabel: UILabel!
    @IBOutlet weak var emailInputField: UITextField!
    @IBOutlet weak var emailIcon: UIImageView!
    @IBOutlet weak var passwordTitle: UILabel!
    @IBOutlet weak var passwordInputField: UITextField!
    @IBOutlet weak var passwordIcon: UIImageView!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    private let disposeBag = DisposeBag()
    private let viewModel: LoginViewModel
    
    init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindViewModel()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func bindViewModel() {
        let userNameSubject = PublishSubject<String>()
        let passwrodSubject = PublishSubject<String>()
        let loginTapSubject = PublishSubject<Void>()
        let movetoRegister = PublishSubject<Void>()
        
        let input = LoginViewModel.Input(userName: userNameSubject, passWord: passwrodSubject, loginTap: loginTapSubject, registerTap: movetoRegister)

        let output = viewModel.transForm(input: input)
        
        emailInputField.rx.text.orEmpty
            .bind(to: userNameSubject)
            .disposed(by: disposeBag)
        
        passwordInputField.rx.text.orEmpty
            .bind(to: passwrodSubject)
            .disposed(by: disposeBag)
        
        loginButton.rx.tap
            .bind(to: loginTapSubject)
            .disposed(by: disposeBag)
        
        registerButton.rx.tap
            .bind(to: movetoRegister)
            .disposed(by: disposeBag)
        
        output.isLoginButtonEnabled
            .drive(loginButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        output.isLoginButtonEnabled
            .map { $0 ? Asset.Colors._4356B4.color : Asset.Colors.cacaca.color }
            .drive(onNext: { [weak self] color in
                
                self?.loginButton.backgroundColor = color
            })
            .disposed(by: disposeBag)
    }
}
