//
//  LoginViewModel.swift
//  AweSome Chat
//
//  Created by pham hong duy on 02/06/2024.
//

import Foundation
import RxSwift
import RxCocoa
import FirebaseAuth

struct LoginCoordinatorActions {
    let showRegister: () -> ()
    let showForgotPasscode: () -> ()
    let showHome: () -> ()
}

class LoginViewModel {
    
    private var coordinator: LoginCoordinatorActions?
    private let disposeBag = DisposeBag()
    
    init(coordinator: LoginCoordinatorActions) {
        self.coordinator = coordinator
    }
    
    func transForm(input: Input) -> Output {
        let credentials = Observable.combineLatest(input.userName, input.passWord) {
            return !$0.isEmpty && !$1.isEmpty
        }
        
        let isLoginButtonEnabled = credentials
            .asDriver(onErrorJustReturn: false)
        
        let loginResult = input.loginTap.withLatestFrom(Observable.combineLatest(credentials, input.userName, input.passWord))
            .flatMapLatest { (isValid, email, password) -> Observable<(Bool, String, String)> in
                return Observable.just((isValid, email, password))
            }
            .asDriver(onErrorJustReturn: (false, "", ""))
        
        let navigateToRegister = input.registerTap
            .asDriver(onErrorJustReturn: ())
        
        loginResult
            .drive(onNext: { [weak self] (success, email, password) in
                guard let wSelf = self else { return }
                if success {
                    Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
                        guard let error = error else {
//                            wSelf.coordinator.showPopUpResultLogin(isSuccess: true, descriptionError: nil)
                            return
                        }
                        wSelf.coordinator?.showHome()
                    }
                }
            })
            .disposed(by: disposeBag)
        
        navigateToRegister
            .drive(onNext: { [weak self] in
                self?.coordinator?.showRegister()
            })
            .disposed(by: disposeBag)
        
        return Output(isLoginButtonEnabled: isLoginButtonEnabled,
                      navigateToRegister: navigateToRegister)
    }
    
}

extension LoginViewModel {
    struct Input {
        let userName: PublishSubject<String>
        let passWord: PublishSubject<String>
        let loginTap: PublishSubject<Void>
        let registerTap: PublishSubject<Void>
    }
    
    struct Output {
        let isLoginButtonEnabled: Driver<Bool>
        let navigateToRegister: Driver<Void>
    }
}
