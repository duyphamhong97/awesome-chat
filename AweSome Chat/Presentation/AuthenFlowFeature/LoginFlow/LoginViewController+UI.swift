//
//  LoginViewController+UI.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/2/24.
//

import Foundation
import UIKit

extension LoginViewController {
    func setupUI() {
        setupTitle()
        setupAtribute()
    }
    
    func setupTitle() {
        avatarImage.image = Asset.Assets.icAvatarMail.image
        titleLabel.text = L10n.Login.awesomeChat
        screenNameLabel.text = L10n.Login.login
        titleEmaillabel.text = L10n.Login.email
        passwordTitle.text = L10n.Login.password
        forgotPasswordButton.setTitle(L10n.Login.forgotPass, for: .normal)
        loginButton.setTitle(L10n.Login.login.uppercased(), for: .normal)
        registerButton.setAttributedTitle(
            fullText: L10n.Login.noAccount + " \(L10n.Login.register)",
            parts: [
                (text:  L10n.Login.noAccount, font: .typography(token: .latoRegular, size: 14), color: Asset.Colors._999999.color),
                (text: L10n.Login.register, font: .typography(token: .latoBold, size: 14), color: Asset.Colors._4356B4.color)
            ]
        )
    }
    
    func setupAtribute() {
        emailIcon.image = Asset.Assets.icMail.image
        passwordIcon.image = Asset.Assets.icKey.image
        forgotPasswordButton.setTitleColor(Asset.Colors._4356B4.color, for: .normal)
        forgotPasswordButton.titleLabel?.font = .typography(token: .latoBold, size: 14)
        loginButton.setTitleColor(Asset.Colors.ffffff.color, for: .normal)
        loginButton.titleLabel?.font = .typography(token: .latoBold, size: 16)
    }
}
