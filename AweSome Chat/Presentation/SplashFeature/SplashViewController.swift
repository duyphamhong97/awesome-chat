//
//  ViewController.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 5/31/24.
//

import UIKit
import RxSwift

struct SplashCoordinatorActions {
    let showLogin: () -> ()
}

class SplashViewController: UIViewController {
    
    private var coordinatorActions: SplashCoordinatorActions?
    private let disposeBag = DisposeBag()
    
    init(actions: SplashCoordinatorActions) {
        self.coordinatorActions = actions
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigateToLogin()
    }
    
    func navigateToLogin() {
        Observable.just(())
            .delay(.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.coordinatorActions?.showLogin()
            })
            .disposed(by: disposeBag)
    }
    
}
