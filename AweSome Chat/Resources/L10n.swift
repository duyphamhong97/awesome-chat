// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return prefer_self_in_static_references

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  internal enum Login {
    /// I agree with
    internal static let agree = L10n.tr("Localizable", "login.agree", fallback: "I agree with")
    /// and
    internal static let and = L10n.tr("Localizable", "login.and", fallback: "and")
    /// Experience Awesome chat
    internal static let awesomeChat = L10n.tr("Localizable", "login.awesome_chat", fallback: "Experience Awesome chat")
    /// conditions
    internal static let condition = L10n.tr("Localizable", "login.condition", fallback: "conditions")
    /// EMAIL
    internal static let email = L10n.tr("Localizable", "login.email", fallback: "EMAIL")
    /// Forgot password?
    internal static let forgotPass = L10n.tr("Localizable", "login.forgot_pass", fallback: "Forgot password?")
    /// Do you have an account?
    internal static let haveAccount = L10n.tr("Localizable", "login.have_account", fallback: "Do you have an account?")
    /// Login
    internal static let login = L10n.tr("Localizable", "login.login", fallback: "Login")
    /// Login now
    internal static let loginNow = L10n.tr("Localizable", "login.login_now", fallback: "Login now")
    /// Haven't account?
    internal static let noAccount = L10n.tr("Localizable", "login.no_account", fallback: "Haven't account?")
    /// Password
    internal static let password = L10n.tr("Localizable", "login.password", fallback: "Password")
    /// Register now
    internal static let register = L10n.tr("Localizable", "login.register", fallback: "Register now")
    /// Register
    internal static let registerTitle = L10n.tr("Localizable", "login.register_title", fallback: "Register")
    /// terms
    internal static let term = L10n.tr("Localizable", "login.term", fallback: "terms")
    /// Username
    internal static let username = L10n.tr("Localizable", "login.username", fallback: "Username")
    internal enum Email {
      /// Localizable.strings
      ///   AweSome Chat
      /// 
      ///   Created by PHAM HONG DUY on 5/31/24.
      internal static let title = L10n.tr("Localizable", "login.email.title", fallback: "Email")
    }
    internal enum Password {
      /// Password
      internal static let title = L10n.tr("Localizable", "login.password.title", fallback: "Password")
    }
  }
  internal enum Register {
    internal enum RegisterFailed {
      /// Register failed
      internal static let title = L10n.tr("Localizable", "register.register_failed.title", fallback: "Register failed")
    }
    internal enum RegisterSuccess {
      /// Register success
      internal static let title = L10n.tr("Localizable", "register.register_success.title", fallback: "Register success")
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg..., fallback value: String) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: value, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
