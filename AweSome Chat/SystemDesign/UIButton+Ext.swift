//
//  UIButton+Ext.swift
//  AweSome Chat
//
//  Created by pham hong duy on 02/06/2024.
//

import Foundation
import UIKit

extension UIButton {
    func setAttributedTitle(fullText: String, parts: [(text: String, font: UIFont, color: UIColor)]) {
        let attributedString = NSMutableAttributedString(string: fullText)
        
        for part in parts {
            let range = (fullText as NSString).range(of: part.text)
            attributedString.addAttribute(.font, value: part.font, range: range)
            attributedString.addAttribute(.foregroundColor, value: part.color, range: range)
        }
        
        self.setAttributedTitle(attributedString, for: .normal)
    }
}
