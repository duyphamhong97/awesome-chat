//
//  UIFont+Ext.swift
//  AweSome Chat
//
//  Created by pham hong duy on 02/06/2024.
//

import Foundation
import UIKit

public extension UIFont {
    static func typography(token: TypographyType, size: CGFloat) -> UIFont {
        return token.font(size: size)
    }
}

public enum TypographyType {
    case latoRegular
    case latoBold
}

extension TypographyType {
    func font(size: CGFloat) -> UIFont {
        switch self {
        case .latoBold:
            guard let latoBold = UIFont(name: "Lato-Bold", size: size) else {
                fatalError("""
                        Failed to load the "Lato-Bold" font.
                        Make sure the font file is included in the project and the font name is spelled correctly.
                        """
                )
            }
            return latoBold
        case .latoRegular:
            guard let latoRegular = UIFont(name: "Lato-Regular", size: size) else {
                fatalError("""
                        Failed to load the "Lato-Regular" font.
                        Make sure the font file is included in the project and the font name is spelled correctly.
                        """
                )
            }
            return latoRegular
        }
    }
}

