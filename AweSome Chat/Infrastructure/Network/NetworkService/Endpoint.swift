//
//  Endpoint.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import Foundation

protocol EndpointType {
    var method: HTTPMethod { get }
    var baseURL: String { get }
    var path: String { get set }
    var params: HTTPParams? { get set }
}

class Endpoint: EndpointType {
    var method: HTTPMethod
    var baseURL: String
    var path: String
    var params: HTTPParams?
    
    init(method: HTTPMethod, baseURL: String, path: String, params: HTTPParams? = nil) {
        self.method = method
        self.baseURL = baseURL
        self.path = path
        self.params = params
    }
}
