////
////  NetworkService.swift
////  AweSome Chat
////
////  Created by PHAM HONG DUY on 6/16/24.
////
//
//import Foundation
//
//struct NetworkError: Error {
//    let error: Error?
//    let statusCode: Int?
//    let data: Data?
//    
//    init(error: Error? = nil, statusCode: Int? = nil, data: Data? = nil) {
//        self.error = error
//        self.statusCode = statusCode
//        self.data = data
//    }
//}
//
//struct RequestConfig {
//    var uploadTask: Bool?
//    var autoValidation: Bool?
//    
//    init(uploadTask: Bool? = nil, autoValidation: Bool? = nil) {
//        self.uploadTask = uploadTask
//        self.autoValidation = autoValidation
//    }
//}
//
//protocol NetworkServiceAsyncAwaitType {
//    var urlSession: URLSession { get }
//    
//    func request(_ endpoiny: EndpointType, config: RequestConfig?) async throws -> Data
//    func request<T: Decodable>(_ endpoint: EndpointType, type: T.Type, config: RequestConfig?) async throws -> T
//    
//    func requestWithStatusCode(_ endpoint: EndpointType, config: RequestConfig?) async throws -> (result: Data, statusCode: Int?)
//    func requestWithStatusCode<T: Decodable>(_ endpoint: EndpointType, type: T.Type, config: RequestConfig?) async throws -> (result: T, statusCode: Int?)
//}
//
//protocol NetworkServiceCallbacksType {
//    var urlSesstion: URLSession { get }
//    
//    func request(_ endpoint: EndpointType, config: RequestConfig?, completion: @escaping (Result<Data?, NetworkError>) -> Void) -> NetworkCancellable?
//    func request<T: Decodable>(_ endpoint: EndpointType, type: T.Type, config: RequestConfig?, completion: @escaping (Result<T, NetworkError>) -> Void) -> NetworkCancellable?
//    
//    func requestWithStatusCode(_ endpoint: EndpointType, config: RequestConfig?, completion: @escaping (Result<(result: Data?, statusCode: Int?), NetworkError>) -> Void) -> NetworkCancellable?
//    func requestWithStatusCode<T: Decodable>(_ endpoint: EndpointType, type: T.Type, config: RequestConfig?, completion: @escaping (Result<(result: Data?, statusCode: Int?), NetworkError>) -> Void) -> NetworkCancellable?
//}
//
//typealias NetworkSericeType = NetworkServiceAsyncAwaitType & NetworkServiceCallbacksType
//
//class NetworkService: NetworkSericeType {
//    
//    let urlSession: URLSession
//    var autoValidation: Bool
//    private var defaultConfig = RequestConfig(uploadTask: false, autoValidation: true)
//    
//    init(urlSession: URLSession = URLSession.shared, autoValidation: Bool = true) {
//        self.urlSession = urlSession
//        self.autoValidation = autoValidation
//    }
//    
//    private func log(_ str: String) {
//        #if DEBUG
//        print(str)
//        #endif
//    }
//    
//    @discardableResult
//    private func validate(_ statusCode: Int?, data: Data? = nil, requestValidation: Bool) throws -> Bool {
//        if !requestValidation { return true } // If validation is disabled for a given request (by default is enabled), then this automatic validation will not occur, even if global validation is enabled
//        if !autoValidation { return true } // Next, we check the global validation rule: when validation is enabled for a given request (this is by default), but global validation is disabled, then this automatic validation will not occur
//        guard statusCode != nil, !(statusCode! >= 400 && statusCode! <= 599) else {
//            throw NetworkError(statusCode: statusCode, data: data)
//        }
//        return true
//    }
//    
//    // MARK: - asyn/await API
//    
//    func request(_ endpoiny: EndpointType, config: RequestConfig? = nil) async throws -> Data {
//        
//    }
//    
//    func request<T>(_ endpoint: EndpointType, type: T.Type, config: RequestConfig? = nil) async throws -> T where T : Decodable {
//        
//    }
//    
//    func requestWithStatusCode(_ endpoint: EndpointType, config: RequestConfig? = nil) async throws -> (result: Data, statusCode: Int?) {
//        
//    }
//    
//    func requestWithStatusCode<T>(_ endpoint: EndpointType, type: T.Type, config: RequestConfig? = nil) async throws -> (result: T, statusCode: Int?) where T : Decodable {
//        
//    }
//    
//    var urlSesstion: URLSession
//    
//    func request(_ endpoint: EndpointType, config: RequestConfig?, completion: @escaping (Result<Data?, NetworkError>) -> Void) -> NetworkCancellable? {
//        
//    }
//    
//    func request<T>(_ endpoint: EndpointType, type: T.Type, config: RequestConfig? = nil, completion: @escaping (Result<T, NetworkError>) -> Void) -> NetworkCancellable? where T : Decodable {
//        
//    }
//    
//    func requestWithStatusCode(_ endpoint: EndpointType, config: RequestConfig? = nil, completion: @escaping (Result<(result: Data?, statusCode: Int?), NetworkError>) -> Void) -> NetworkCancellable? {
//        
//    }
//    
//    func requestWithStatusCode<T>(_ endpoint: EndpointType, type: T.Type, config: RequestConfig? = nil, completion: @escaping (Result<(result: Data?, statusCode: Int?), NetworkError>) -> Void) -> NetworkCancellable? where T : Decodable {
//        
//    }
//}
//
//protocol NetworkCancellable {
//    func cancel()
//}
