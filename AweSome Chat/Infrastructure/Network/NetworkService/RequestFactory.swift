//
//  RequestFactory.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import Foundation

public enum HTTPMethod: String {
    case GET
    case POST
    case PUSH
    case PATH
    case DELETE
}
