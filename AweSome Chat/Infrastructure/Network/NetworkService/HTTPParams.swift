//
//  HTTPParams.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import Foundation

class HTTPParams {
    var httpBody: Any?
    var cacheBody: URLRequest.CachePolicy?
    var timeoutInterval: TimeInterval?
    var headerValue: [(value: String, forHTTPHeaderValueField: String)]?
    
    init(httpBody: Any? = nil, cacheBody: URLRequest.CachePolicy? = nil, timeoutInterval: TimeInterval? = nil, headerValue: [(value: String, forHTTPHeaderValueField: String)]? = nil) {
        self.httpBody = httpBody
        self.cacheBody = cacheBody
        self.timeoutInterval = timeoutInterval
        self.headerValue = headerValue
    }
}

enum HTTPReader: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case accept = "Accept"
    case acceptEncoding = "Accept-Encoding"
    case acceptLanguage = "Accept-Language"
    case connection = "Connection"
}

enum ContentType: String {
    case appicationJson = "application/json"
    case applicationFormUrlencoded = "application/x-www-form-urlencoded"
    case multipartFormData = "multipart/form-data"
    case textPlain = "text/plain"
    case applicationXML = "application/xml"
    case applicationQuery = "application/query"

}
