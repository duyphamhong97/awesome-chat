//
//  DataTransferService.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import Foundation

enum DataTransferError: Error {
    case noResponse
    case parsing(Error)
    case network
}

protocol DataTransferService {
    typealias CompletionHandler<T> = (Result<T, DataTransferError>) -> Void
}
