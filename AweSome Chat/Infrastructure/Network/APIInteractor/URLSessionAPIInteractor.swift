////
////  URLSessionAPIInteractor.swift
////  AweSome Chat
////
////  Created by PHAM HONG DUY on 6/16/24.
////
//
//import Foundation
//
//class URLSessionAPIInteractor: ApiInteractor {
//    
//    let urlSesstionAdapter: NetworkService
//    
//    init(with networkService: NetworkService) {
//        self.urlSesstionAdapter = networkService
//    }
//    
//    func request(_ endpoint: EndpointType) async throws -> Data {
//        do {
//            return try await urlSesstionAdapter.request(endpoint)
//        } catch {
//            throw handleError(error)
//        }
//    }
//    
//    func request<T>(_ endPoint: EndpointType, type: T.Type) async throws -> T where T : Decodable {
//        do {
//            return try await urlSesstionAdapter.request(endPoint, type: type)
//        } catch {
//            throw handleError(error)
//        }
//    }
//    
//    func fetchFile(url: URL) async throws -> Data? {
//        
//    }
//    
//    private func handleError(_ error: Error) -> AppError {
//        if let networkError = error as? NetworkError, let statusCode = networkError.statusCode {
//            return (400...599).contains(statusCode) ?
//                .server(networkError.error, status: statusCode, data: networkError.data) :
//                .unexpected(networkError.error, statusCode: statusCode, data: networkError.data)
//        }
//        return .unexpected(error)
//    }
//}
