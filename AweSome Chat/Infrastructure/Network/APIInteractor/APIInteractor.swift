//
//  APIInteractor.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import Foundation

protocol ApiInteractor {
    func request(_ endpoint: EndpointType) async throws -> Data
    func request<T: Decodable>(_ endPoint: EndpointType, type: T.Type) async throws -> T
    func fetchFile(url: URL) async throws -> Data?
}
