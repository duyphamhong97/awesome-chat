//
//  DefaultAuthenRepository.swift
//  AweSome Chat
//
//  Created by PHAM HONG DUY on 6/16/24.
//

import Foundation
import FirebaseAuth

class DefaultAuthenRepository: AuthenRepository {
    
    let apiInteractor: ApiInteractor
    
    init(apiInteractor: ApiInteractor) {
        self.apiInteractor = apiInteractor
    }
    
    func registerAccount(_ credential: CredentialQuery) async -> AuthenDataResult {
        do {
            let result = try await Auth.auth().createUser(withEmail: credential.email, password: credential.password)
            return .success(result.user.uid.data(using: .utf8))
        } catch {
            if error is AppError {
                return .failure(error as! AppError)
            }
            return .failure(AppError.unexpected(error))
        }
    }
    
    func login(_ credential: CredentialQuery) async -> AuthenDataResult {
        do {
            let result = try await Auth.auth().signIn(withEmail: credential.email, password: credential.password)
            return .success(result.user.uid.data(using: .utf8))
        } catch {
            if error is AppError {
                return .failure(error as! AppError)
            }
            return .failure(AppError.unexpected(error))
        }
    }
}
